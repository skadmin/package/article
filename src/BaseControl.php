<?php

declare(strict_types=1);

namespace Skadmin\Article;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE = 'article';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-file-word']),
            'items'   => ['overview'],
        ]);
    }
}
