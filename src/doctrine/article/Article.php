<?php

declare(strict_types=1);

namespace Skadmin\Article\Doctrine\Article;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

use function boolval;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Article
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\LastUpdate;
    use Entity\Code;

    #[ORM\Column(options: ['default' => false])]
    private bool $isSystem = false;

    public function update(string $name, string $content, bool $isActive, string $lastUpdateAuthor): void
    {
        $this->name             = $name;
        $this->content          = $content;
        $this->lastUpdateAuthor = $lastUpdateAuthor;

        $this->setIsActive($isActive);
    }

    public function isSystem(): bool
    {
        return $this->isSystem;
    }

    public function setIsActive(bool|int $isActive): void
    {
        $this->isActive = boolval($isActive);
    }

    public function updateWebalize(string $webalize): void
    {
        $this->webalize = $webalize;
    }
}
