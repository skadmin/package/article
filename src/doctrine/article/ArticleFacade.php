<?php

declare(strict_types=1);

namespace Skadmin\Article\Doctrine\Article;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class ArticleFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Article::class;
    }

    public function create(string $name, string $content, bool $isActive, string $lastUpdateAuthor): Article
    {
        return $this->update(null, $name, false, $content, $isActive, $lastUpdateAuthor);
    }

    public function update(?int $id, string $name, bool $changeWebalize, string $content, bool $isActive, string $lastUpdateAuthor): Article
    {
        $article = $this->get($id);
        $article->update($name, $content, $isActive, $lastUpdateAuthor);

        if (! $article->isLoaded()) {
            $article->setWebalize($this->getValidWebalize($name));
        } elseif ($changeWebalize) {
            $article->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }

    public function get(?int $id = null): Article
    {
        if ($id === null) {
            return new Article();
        }

        $user = parent::get($id);

        if ($user === null) {
            return new Article();
        }

        return $user;
    }

    public function findByWebalize(string $webalize): ?Article
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByCode(string $code): ?Article
    {
        $criteria = ['code' => $code];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    /**
     * @return Article[]
     */
    public function findByPhrase(string $phrase): array
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->orX(
            Criteria::expr()->contains('a.name', $phrase),
            Criteria::expr()->contains('a.webalize', $phrase),
            Criteria::expr()->contains('a.content', $phrase)
        ));

        $qb->addCriteria($criteria);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Article[]
     */
    public function findForSitemap(): array
    {
        return $this->getAll(true, false);
    }

    /**
     * @return Article[]
     */
    public function getAll(bool $onlyActive = false, ?bool $isSystem = null): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        if ($isSystem !== null) {
            $criteria['isSystem'] = $isSystem;
        }

        $orderBy = ['name' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
