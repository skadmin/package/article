<?php

declare(strict_types=1);

namespace Skadmin\Article\Utils;

use App\Model\System\Callback;
use Nette\Utils\Html;
use Nette\Utils\Validators;
use ReflectionMethod;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;

use function call_user_func;
use function intval;
use function method_exists;

class UtilsArticle
{
    private ArticleFacade $facadeArticle;

    public function __construct(ArticleFacade $facadeArticle)
    {
        $this->facadeArticle = $facadeArticle;
    }

    public function getArticleByCode(string $identifier, ?string $titleTag = null, array $attrs = []): ?Html
    {
        if (Validators::isNumericInt($identifier)) {
            $article = $this->facadeArticle->get(intval($identifier));
        } else {
            $article = $this->facadeArticle->findByCode($identifier);
        }

        return $this->getHtmlArticle($article, $titleTag, $attrs);
    }

    public function getArticleByWebalize(string $identifier, ?string $titleTag = null, array $attrs = []): ?Html
    {
        if (Validators::isNumericInt($identifier)) {
            $article = $this->facadeArticle->get(intval($identifier));
        } else {
            $article = $this->facadeArticle->findByWebalize($identifier);
        }

        return $this->getHtmlArticle($article, $titleTag, $attrs);
    }

    private function getHtmlArticle(?Article $article, ?string $titleTag, array $attrs): ?Html
    {
        if ($article === null || ! $article->isLoaded()) {
            return null;
        }

        if (method_exists(Callback::class, 'getHtmlArticle')) { //@phpstan-ignore-line
            $function = new ReflectionMethod(Callback::class, 'getHtmlArticle');
            if ($function->getNumberOfParameters() === 3) {
                return call_user_func([Callback::class, 'getHtmlArticle'], $article, $titleTag, $attrs);
            }

            return call_user_func([Callback::class, 'getHtmlArticle'], $article, $titleTag);
        }

        $html = new Html();
        if ($titleTag !== null) {
            if (! isset($attrs['class'])) {
                $attrs['class'] = 'article-title';
            }

            $html->addHtml(Html::el($titleTag, $attrs)
                ->setText($article->getName()));
        }

        $html->addHtml($article->getContent());

        return $html;
    }
}
