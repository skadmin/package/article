<?php

declare(strict_types=1);

namespace Skadmin\Article\Components\Admin;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

class Detail extends TemplateControl
{
    use APackageControl;

    private ArticleFacade $facade;
    private Article       $article;

    public function __construct(int $id, ArticleFacade $facade, Translator $translator)
    {
        parent::__construct($translator);
        $this->facade = $facade;

        $this->article = $this->facade->get($id);
        $this->isModal = true;
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('article.detail.title - %s', $this->article->getName());
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');

        $template->article = $this->article;

        $template->drawBox = $this->drawBox;

        $template->render();
    }
}
