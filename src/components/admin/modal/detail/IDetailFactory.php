<?php

declare(strict_types=1);

namespace Skadmin\Article\Components\Admin;

interface IDetailFactory
{
    public function create(int $id): Detail;
}
