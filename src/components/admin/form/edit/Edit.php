<?php

declare(strict_types=1);

namespace Skadmin\Article\Components\Admin;

use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Article\BaseControl;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private ArticleFacade $facade;
    private User          $user;
    private Article       $article;

    public function __construct(?int $id, ArticleFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;
        $this->user      = $this->loggedUser->getIdentity(); //@phpstan-ignore-line

        $this->article = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->article->isLoaded()) {
            return new SimpleTranslation('article.edit.title - %s', $this->article->getName());
        }

        return 'article.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->article->isLoaded()) {
            $article = $this->facade->update($this->article->getId(), $values->name, $values->changeWebalize, $values->content, $values->isActive, $this->user->getFullName());
            $this->onFlashmessage('form.article.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $article = $this->facade->create($values->name, $values->content, $values->isActive, $this->user->getFullName());
            $this->onFlashmessage('form.article.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $article->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->article = $this->article;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.article.edit.name')
            ->setRequired('form.article.edit.name.req');
        $form->addTextArea('content', 'form.article.edit.content');
        $form->addCheckbox('isActive', 'form.article.edit.is-active')
            ->setDefaultValue(true);

        // EXTEND
        if ($this->article->isLoaded()) {
            $form->addCheckbox('changeWebalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.article.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // BUTTON
        $form->addSubmit('send', 'form.article.edit.send');
        $form->addSubmit('sendBack', 'form.article.edit.send-back');
        $form->addSubmit('back', 'form.article.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->article->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->article->getName(),
            'content'  => $this->article->getContent(),
            'isActive' => $this->article->isActive(),
        ];
    }
}
