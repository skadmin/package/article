<?php

declare(strict_types=1);

namespace Skadmin\Article\Components\Admin;

interface IEditFactory
{
    public function create(?int $id = null): Edit;
}
