<?php

declare(strict_types=1);

namespace Skadmin\Article\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Callback;
use App\Model\System\Constant;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Article\BaseControl;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function is_array;
use function method_exists;

class Overview extends GridControl
{
    use APackageControl;

    private ArticleFacade $facade;

    public function __construct(ArticleFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'article.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.article.overview.name')
            ->setRenderer(function (Article $article): Html {
                if (method_exists(Callback::class, 'getLinkFor')) { // @phpstan-ignore-line
                    $linkData = Callback::getLinkFor($article, $article->getWebalize()); // @phpstan-ignore-line

                    if (is_array($linkData)) {
                        $frontLink = Html::el('a', [
                            'href'   => $this->getPresenter()->link($linkData['destination'], $linkData['data']),
                            'target' => '_blank',
                        ])->setHtml(Html::el('code', ['class' => 'text-muted small'])->setText($article->getWebalize()));
                    } else {
                        $frontLink = Html::el('code', ['class' => 'text-muted small'])->setText($article->getWebalize());
                    }
                } else {
                    $frontLink = Html::el('code', ['class' => 'text-muted small'])->setText($article->getWebalize());
                }

                $link = $this->getPresenter()->link('Component:default', [
                    'package' => new BaseControl(),
                    'render'  => $this->isAllowed(BaseControl::RESOURCE, 'write') ? 'edit' : 'detail',
                    'id'      => $article->getId(),
                ]);

                $href = Html::el('a', [
                    'href'  => $link,
                    'class' => 'font-weight-bold',
                ])->setText($article->getName());

                $result = new Html();
                $result->addHtml($href)
                    ->addHtml('<br>')
                    ->addHtml($frontLink);

                return $result;
            });
        $grid->addColumnText('isActive', 'grid.article.overview.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);
        $grid->addColumnText('lastUpdateAuthor', 'grid.article.overview.lastUpdateAuthor');
        $grid->addColumnDateTime('lastUpdateAt', 'grid.article.overview.lastUpdateAt')
            ->setFormat('d.m.Y H:i');

        // FILTER
        $grid->addFilterText('name', 'grid.article.overview.name');
        $grid->addFilterSelect('isActive', 'grid.article.overview.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();
        $grid->addFilterText('lastUpdateAuthor', 'grid.article.overview.lastUpdateAuthor');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.article.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        $grid->addAction('detail', 'grid.article.overview.action.detail', 'Component:default', ['id' => 'id'])->addParameters([
            'package' => new BaseControl(),
            'render'  => 'detail',
        ])->setIcon('eye')
            ->setClass('btn btn-xs btn-outline-primary ajax');

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.article.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        // IF USER ALLOWED WRITE

        // ALLOW

        return $grid;
    }
}
