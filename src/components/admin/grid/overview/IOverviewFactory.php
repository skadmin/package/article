<?php

declare(strict_types=1);

namespace Skadmin\Article\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
