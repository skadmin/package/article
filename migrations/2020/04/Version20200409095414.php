<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200409095414 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'article.overview', 'hash' => '3a668e859eda86f7b112c04b280b4ce4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Články', 'plural1' => '', 'plural2' => ''],
            ['original' => 'article.overview.title', 'hash' => '399573a28dbc3b0c903bcbb9eba2e40f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Články|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.article.overview.action.new', 'hash' => 'fdaa69b8e11643e30f838f23d9a9e9bb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit článek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.article.overview.name', 'hash' => '0bd1bce91a90feb1b33fb7578df1f5b9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.article.overview.is-active', 'hash' => '20f3de3d44ff6ceec649e7be0c707fd7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.article.overview.lastUpdateAuthor', 'hash' => 'd23a1ce287d9c0f865c933e4bbd33b7d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poslední autor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.article.overview.lastUpdateAt', 'hash' => '5a450c8854cf568f8fd4821a0b5264a9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poslední změna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.article.overview.action.edit', 'hash' => '9a62faff2984f90f1da70a0ca01b6632', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.article.overview.action.detail', 'hash' => 'f6e12fab713adb5734c8dcffde2c171a', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'article.detail.title - %s', 'hash' => 'df371062a891d40de0823856b3855d7c', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s', 'plural1' => '', 'plural2' => ''],
            ['original' => 'article.detail.is-active', 'hash' => '2548f39902e1da2d15c322f50e4cb74a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'article.detail.last-update-at', 'hash' => '4b5aea8a7695a4e50fb816e69299b059', 'module' => 'admin', 'language_id' => 1, 'singular' => 'poslední změna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'article.edit.title', 'hash' => 'c733f038d435ea88db118bea328f694c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení článku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.name', 'hash' => '0d37aef3f50d3e176bab1bc854a7a17f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.name.req', 'hash' => '925384ca9f64fb6e70af746e6705f6ab', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.content', 'hash' => '710561dcbf81b0395bbe869735b3ac32', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.is-active', 'hash' => '21649802f9b84b20094484f83d33e3b4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.send', 'hash' => 'b78d3a50e9d7435379c1e8cb51edd6fc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.send-back', 'hash' => '156a83fd6ba11af4622979d710ed6b4d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.back', 'hash' => 'ba5203d67475f793748215afb332e115', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'article.edit.title - %s', 'hash' => 'd18e867be498ae33208eddbe2461eae2', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace článku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.flash.success.update', 'hash' => 'a973c423f8d1662d41f655a86531c68e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Článek byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.article.edit.flash.success.create', 'hash' => 'ecc43379e69b4174d743dc496159e227', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Článek byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.article.title', 'hash' => 'a4e83c8538e49aea7320eb16ebedd192', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Články', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.article.description', 'hash' => '65c615c4a3da663621f50b7983f171e9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat články', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
