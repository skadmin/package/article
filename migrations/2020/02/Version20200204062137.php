<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200204062137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $now = (new DateTime())->format('Y-m-d H:i:s');

        /* DATA */
        $articles = [
            [
                'is_active'          => 1,
                'webalize'           => 'error-4xx',
                'name'               => 'Oops... - E4xx',
                'content'            => '<p>Your browser sent a request that this server could not understand or process.</p>',
                'last_update_author' => 'Cron',
                'last_update_at'     => $now,
            ],
            [
                'is_active'          => 1,
                'webalize'           => 'error-403',
                'name'               => 'Access Denied - E403',
                'content'            => '<p>You do not have permission to view this page. Please try contact the web
site administrator if you believe you should be able to view this page.</p>',
                'last_update_author' => 'Cron',
                'last_update_at'     => $now,
            ],
            [
                'is_active'          => 1,
                'webalize'           => 'error-404',
                'name'               => 'Page Not Found - E404',
                'content'            => '<p>The page you requested could not be found. It is possible that the address is
incorrect, or that the page no longer exists. Please use a search engine to find
what you are looking for.</p>',
                'last_update_author' => 'Cron',
                'last_update_at'     => $now,
            ],
            [
                'is_active'          => 1,
                'webalize'           => 'error-405',
                'name'               => 'Method Not Allowed - E405',
                'content'            => '<p>The requested method is not allowed for the URL.</p>',
                'last_update_author' => 'Cron',
                'last_update_at'     => $now,
            ],
        ];

        foreach ($articles as $article) {
            $this->addSql(
                'INSERT INTO article (is_active, webalize, name, content, last_update_author, last_update_at) VALUES (:is_active, :webalize, :name, :content, :last_update_author, :last_update_at)',
                $article
            );
        }
    }

    public function down(Schema $schema): void
    {
    }
}
